# KIT-CA Portal

### Development

In order to use the docker-compose setup, you'll need to

* Create `dev/.env` with the content
```
BACKEND_REPO_PATH=/path/where/backend/is/checked/out
```
* The backend config file in `$HOME/.config/`
* `podman-compose` or `docker-compose` installed. If you want to use podman, also make sure that `podman-dnsname` is installed.

Start the dev environment by running `dev/compose.sh` through the terminal or the provided JetBrains run configuration.

Shut down the dev environment by running `docker-compose down` on your terminal of choice.