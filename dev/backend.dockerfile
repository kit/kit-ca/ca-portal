FROM docker.io/golang:latest

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get install -y bsdmainutils && rm -rf /var/lib/apt/lists/*

COPY backend-files /

# Root CA created with `openssl req -x509 -nodes -new -newkey rsa:4096 -keyout PortalDevRootCA.key -out PortalDevRootCA.pem -subj '/C=DE/O=Karlsruhe Institute of Technology/CN=KIT-CA Portal Dev Root CA' -addext keyUsage=critical,keyCertSign -addext basicConstraints=critical,CA:true,pathlen:0 -addext subjectKeyIdentifier=hash`
RUN openssl req \
	-x509 \
    -nodes \
    -new \
    -newkey rsa:4096 \
    -keyout /tmp/smime.key \
    -out /tmp/smime.pem \
    -CA /PortalDevRootCA.pem \
    -CAkey /PortalDevRootCA.key \
    -set_serial 0x$(dd if=/dev/urandom count=12 bs=1 | hexdump -e '"%x"') \
    -subj '/C=DE/O=Karlsruhe Institute of Technology/CN=KIT-CA Portal Robot/emailAddress=portal-dev@portal-dev.local' \
    -addext keyUsage=critical,digitalSignature,keyEncipherment,nonRepudiation \
    -addext extendedKeyUsage=emailProtection \
    -addext basicConstraints=CA:false \
    -addext subjectAltName=email:portal-dev@portal-dev.local

RUN mkdir /root/.config/ && ln -s /kit-ca-api-server.toml /root/.config/

ENTRYPOINT ["/backend"]