#!/usr/bin/env bash

if command -v podman-compose > /dev/null; then
	executable=podman-compose
else
	export DOCKER_HOST=unix:///run/user/$UID/podman/podman.sock
	executable=docker-compose
fi

if ! command -v $executable > /dev/null; then
	echo "Neither podman-compose nor docker-compose found."
	exit 1
fi

if ! command -v j2 > /dev/null; then
	echo "Please install j2cli via your package manager or pipx."
	exit 1
fi

$executable down

set -o allexport
source .env
set +o allexport

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
target_dir="$SCRIPT_DIR"/backend-files
cd "$BACKEND_REPO_PATH/cmd/api-server/" || exit 1
go build -o "$target_dir/backend" main.go || { echo "Go build failed."; exit 1; }
cd - > /dev/null || exit 1

j2 backend-files/kit-ca-api-server.toml.j2 > backend-files/kit-ca-api-server.toml

$executable up -d --build

rm backend-files/kit-ca-api-server.toml

if command -v tmux > /dev/null && ! tmux has-session -t portal-dev 2> /dev/null; then
	tmux new-session -d -s portal-dev
	tmux split-window -v
	tmux split-window -v
	tmux select-layout even-vertical
	tmux send-keys -t portal-dev.0 "while true; do podman logs -f portal-dev-backend 2>&1 | tee >(jq -R 'fromjson?'); sleep 1; done" Enter
	tmux send-keys -t portal-dev.1 "while true; do podman logs -f portal-dev-portal; sleep 1; done" Enter
	tmux send-keys -t portal-dev.2 "while true; do podman logs -f portal-dev-caddy | jq -R 'fromjson? | \"[\(.ts | strflocaltime(\"%Y-%m-%dT%H:%M:%S%z\"))] \(.request.remote_ip) \(if .user_id == \"\" then \"-\" else .user_id end) \(.request.method) \(.request.uri) \(.request.proto) \(.status) \(.size)\"'; sleep 1; done" Enter
	echo "You can now tmux a -t portal-dev!"
fi
