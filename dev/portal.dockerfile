FROM docker.io/node:slim

WORKDIR /src
ENTRYPOINT ["/bin/bash", "-c", "yarn install --immutable; yarn run dev"]