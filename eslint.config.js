import js from "@eslint/js";
import pluginVue from "eslint-plugin-vue";
import { FlatCompat } from "@eslint/eslintrc";
import { fileURLToPath } from "node:url";
import path from "node:path";

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);
const compat = new FlatCompat({
	baseDirectory: __dirname,
	recommendedConfig: js.configs.recommended,
});

export default [
	js.configs.recommended,
	...pluginVue.configs["flat/recommended"],
	...compat.extends("@vue/eslint-config-typescript/recommended"),
	...compat.extends("@vue/eslint-config-prettier"),
	{
		files: [
			"**/*.vue",
			"**/*.js",
			"**/*.jsx",
			"**/*.cjs",
			"**/*.mjs",
			"**/*.ts",
			"**/*.tsx",
			"**/*.cts",
			"**/*.mts",
		],
		languageOptions: {
			ecmaVersion: "latest",
		},
		rules: {
			"vue/multi-word-component-names": "off",
		},
	},
];
