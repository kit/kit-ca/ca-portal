import { I18nType } from "@/i18n/type";

export const de: I18nType = {
	$vuetify: {
		stepper: {
			next: "Weiter",
			prev: "Zurück",
			submit: "Absenden",
		},
	},
	dialog: {
		nextButton: "Weiter",
		abortButton: "Abbrechen",
		saveButton: "Speichern",
		closeButton: "Schließen",
		moreInfoButton: "Mehr Info",
	},
	systemMessage: {
		title: "Keine Zertifikatsausstellung möglich",
		message:
			"Sectigo, der Betreiber der Zertifizierungsstelle, über die wir Zertifikate erhalten, hat am 10.01.2025 unseren Zugang gesperrt. Es können deshalb gerade keine Zertifikate beantragt werden, bis der neue Betreiber vollständig in unsere Prozesse integriert ist. Dies kann einige Wochen dauern, wir bitten um Ihr Verständnis und entschuldigen uns für die Umstände.",
	},
	unsupportedLocale: {
		title: "Nicht unterstützte Sprache",
		message:
			"Die im Browser eingestellte Sprache {language} wird noch nicht unterstützt. Es wird daher die Standardsprache {defaultLanguage} angezeigt.",
		contribute: "Übersetzung erstellen",
		dontShowAgain: "Nicht mehr anzeigen",
	},
	home: {
		title: "Zertifikatsportal",
		welcomeMessage: "Willkommen, {name}!",
		sessionExpired: {
			moreInfo: {
				title: "Informationen zum Sitzungsablauf",
			},
			message: "Sitzung abgelaufen, Sie wurden ausgeloggt.",
		},
		identificationSectionHeader: "Identifikation",
		requestSectionHeader: "Zertifikate beantragen",
		manageSectionHeader: "Zertifikate verwalten",
		loginButton: "Anmelden",
		logoutButton: "Abmelden",
		certificates: {
			request: {
				button: {
					enabled: "Beantragen",
					disabled: "Nicht verfügbar",
				},
				personal: {
					name: "Personenzertifikat",
					description:
						"Hier können Personenzertifikate ausgestellt werden. Ein solches Zertifikat ist zum Beispiel zum Signieren oder Verschlüsseln von Mails oder zum Signieren von Dokumenten verwendbar. Ein Personenzertifikat ist durch eine vorherige Identifikation an eine Person gebunden.",
				},
				function: {
					name: "Funktionszertifikat",
					description:
						"Hier können Funktionszertifikate ausgestellt werden. Ein solches Zertifikat ist zum Beispiel zum Signieren und verschlüsseln von Mails in einer geteilten Postfach verwendbar. Für solche Zertifikate ist keine Identifikation nötig, es muss nur nachgewiesen werden, dass Kontrolle über die zu zertifizierende Mailadresse besteht. Es kann daher auch statt einem Personenzertifikat verwendet werden, wenn Sie keinen Wert auf Ihren Namen im Zertifikat legen.",
				},
				server: {
					name: "Serverzertifikat",
					description:
						"Hier können Serverzertifikate ausgestellt werden, die für die sichere Kommunikation zwischen Clients und Servern bzw. zwischen Servern und Servern benötigt werden. Dafür muss die Kontrolle über die Domain nachgewiesen sein. Aktuell kann dafür entweder ACME verwendet oder ein Zertifikatsantrag per Mail durchgeführt werden.",
				},
			},
			manage: {
				button: {
					enabled: "Auswählen",
					disabled: "Nicht verfügbar",
				},
				distribute: {
					name: "Zertifikate verteilen",
					description: "Hier können Zertifikate sicher auf mehrere Geräte verteilt werden.",
				},
			},
		},
		identification: {
			button: {
				enabled: "Auswählen",
				disabled: "Nicht verfügbar",
			},
			view: {
				name: "Identifikation anzeigen",
				description:
					"Hier können bestehende Identifikationen betrachtet und in der Vergangenheit selbst erstellte Identifikationen widerrufen werden.",
			},
			create: {
				name: "Identifikation durchführen",
				description:
					"Hier können neue Identifikationen von KIT-Angehörenden durchgeführt werden. Dazu muss ein offizielles Ausweisdokument geprüft werden.",
			},
		},
	},
	functionPage: {
		backHomeButton: "Zurück zur Hauptseite",
		startOverButton: "Neu Starten",
	},
	createIdent: {
		title: "Identifikation erstellen",
		search: {
			header: "Personensuche nach KIT-Angehörenden",
			label: "Suchbegriff",
			noResults: "Keine Suchergebnisse gefunden",
			result: {
				moreDetails: {
					kitAccountLabel: "KIT-Kürzel",
					commonNameLabel: "Common Name",
					firstNameLabel: "Vorname",
					lastNameLabel: "Nachname",
					departmentLabel: "Abteilung",
					primaryEmailAddressLabel: "Primäre Mailadresse",
					emailAddressLabel: "Mailadresse",
				},
				moreDetailsButton: "Details",
				identifyButton: "Identifizierung",
			},
			rules: {
				noSpacesAllowed: "Leerzeichen sind nicht erlaubt",
				invalidKitAccount: "KIT-Kürzel hat ein ungültiges Format",
			},
		},
		create: {
			title: "Identifikation für {name} durchführen",
			existing: {
				title: "{name} hat bereits gültige Identifizierungen",
				createdAtLabel: "Erstellt am",
				validUntilLabel: "Gültig bis",
				description:
					"Bitte den KIT-Angehörenden fragen, ob dennoch eine neue Identifikation erstellt werden soll. Das kann sinnvoll sein, wenn kürzlich der Name geändert wurde, z.B. aufgrund von erreichtem Doktortitel, Hochzeit, Transition, ...",
			},
			success: {
				title: "Neue Identifikation für {name} erfolgreich erstellt!",
				description: "Es existieren nun die folgenden Identifikationen:",
			},
			error: {
				title: "Beim Erstellen einer neuen Identifikation sind Fehler aufgetreten:",
			},
			documentType: {
				selectTitle: "Dokumententyp",
				unknown: "Unbekannt",
				personalausweis: "Personalausweis",
				passport: "Reisepass",
				aufenthaltstitel: "Aufenthaltstitel",
				other: "Anderes Dokument (bitte Anmerkung anfügen)",
			},
			documentTypeHint:
				"Für Informationen zu einem beliebigem Dokument, z.B. prüfbare Sicherheitsmerkmale, hier klicken und suchen.",
			documentIdentifier: "Dokumentennummer (letzte 5 Zeichen)",
			documentIdentifierHint: {
				title: "Informationen zur Dokumentennummer",
				text: "Dieses Bild zeigt, wo die Dokumentennummer auf der deutschen Variante des Dokuments zu finden ist. Dokumente anderer Länder sind oft ähnlich aufgebaut. Wenn die Dokumentennummer nicht auffindbar ist, bitte die PRADO Datenbank konsultieren.",
			},
			commonName: "Common Name (Anzeigename von Zertifikaten)",
			commonNameHint:
				"Aktuell nicht veränderbar, automatisch aus IDM generiert. Bitte mit Ausweis abgleichen. Bei Diskrepanz Identifikation abbrechen und auf nötige Änderung durch PSE hinweisen.",
			auditorRemarks: "Anmerkungen",
			auditorRemarksEmptyWithDocTypeOtherError:
				'Anmerkungen dürfen nicht leer sein, wenn "Anderes Dokument" der gewählte Dokumententyp ist.',
		},
	},
	request: {
		common: {
			steps: {
				emailAddresses: {
					title: "E-Mail-Adressen",
				},
				checkRequest: {
					title: "Zertifikatsantrag prüfen & Absenden",
					requestInfo: {
						primaryEmailAddress: "Primäre (sichtbare) E-Mail-Adresse des Zertifikats",
						emailAddress: "Zusätzlich im Zertifikat aufgenommene E-Mail-Adresse",
					},
				},
			},
			success: {
				title: "Ihr Zertifikatsantrag wurde erfolgreich eingereicht",
				text: "Ihr Antrag wird in den nächsten Tagen geprüft. Im Anschluss erhalten Sie eine Mail mit weiteren Anweisungen. Wenn Sie möchten, können Sie sich bereits mit den nächsten Schritten vertraut machen.",
				instructionsUrl: "https://ca.kit.edu/p/nutzer#den-sectigo-prozess-korrekt-durchlaufen",
				instructionsButton: "Nächste Schritte",
			},
			error: {
				title: "Ein Fehler ist aufgetreten",
				serviceDeskButton: "Service Desk",
				serviceDeskUrl: "https://www.scc.kit.edu/ueberuns/116.php#Anker0",
			},
		},
		personal: {
			title: "Personenzertifikat beantragen",
			missingIdentification: {
				title: "Fehlende Identifikation",
				text: "Für Sie liegt keine gültige Personenidentifikation vor. Bitte kommen Sie beim SCC Servicedesk mit einem gültigem Ausweisdokument vorbei, damit Sie identifiziert werden können. Anschließend können Sie die Beantragung eines Personenzertifikats erneut versuchen. Alternativ können Sie ein Funktionszertifikat beantragen.",
			},
			steps: {
				emailAddresses: {
					description:
						"Bitte wählen Sie die E-Mail-Adressen aus, die in ihr Zertifikat aufgenommen werden sollen. Ihre Primäradresse wird immer im Zertifikat aufgenommen und kann nicht abgewählt werden.",
				},
				checkRequest: {
					description:
						"Bitte überprüfen Sie Ihren Zertifikatsantrag auf Korrektheit. Sollte etwas nicht stimmen, müssen Ihre Daten im Identitätsmangementsystem korrigiert werden. Bitte wenden Sie sich dafür an den SCC Servicedesk.",
					requestInfo: {
						commonName: "Für das Zertifikat verwendeter Name",
					},
				},
			},
			error: {
				text: "Für Unterstützung können Sie sich an den SCC Service Desk wenden. Wenn Sie keinen Wert darauf legen, dass Ihr Name im Zertifikat auftaucht, können Sie alternativ ein Funktionszertifikat beantragen.",
				functionalInsteadButton: "Funktionszertifikat",
			},
		},
		functional: {
			title: "Funktionszertifikat beantragen",
			steps: {
				emailAddresses: {
					description:
						"Bitte geben Sie die E-Mail-Adressen ein, die in das Zertifikat aufgenommen werden sollen. Alle E-Mail-Adressen müssen für uns nachprüfbar zum gleichen Postfach gehören. Für E-Mail-Adressen unterschiedlicher Postfächer müssen gretrennte Zertifikate beantragt werden. Bei Funktionspostfächern muss der aktuell im CA-Portal angemeldete Account nachweislich Zugriff auf das Postfach haben oder ein IT-Beauftragter der dem Postfach zugeordneten OE sein.",
					emailAddressTextFieldLabel: "E-Mail-Adresse",
					addAnotherButton: "Weitere E-Mail-Addresse hinzufügen",
					addAnotherButtonShort: "Hinzufügen",
					rules: {
						emailAddressEmpty: "Die E-Mail-Adresse darf nicht leer sein",
						emailAddressDoesNotContainExactlyOneAtSign:
							"Die E-Mail-Adresse muss genau ein {'@'} beinhalten",
						emailAddressEndsWithAtSign: "Die E-Mail-Adresse darf nicht mit einem {'@'} enden",
						emailAddressContainsWhitespace:
							"Die E-Mail-Adresse darf keine Leerzeichen oder anderen Whitespace enthalten",
						emailAddressContainsNonASCII:
							"Die E-Mail-Adresse darf nur ASCII-Zeichen enthalten (keine Umlaute, Emojis, ...)",
						emailAddressAlreadyExists: "Die E-Mail-Adresse wurde mehrfach angegeben",
					},
				},
				checkRequest: {
					description:
						"Bitte überprüfen Sie Ihren Zertifikatsantrag auf Korrektheit. Sollte etwas nicht stimmen, gehen Sie einen Schritt zurück und korrigieren Sie Ihre Eingaben.",
				},
			},
			error: {
				text: "Für Unterstützung können Sie sich an den SCC Service Desk wenden.",
			},
		},
		server: {
			instructionsUrl: "https://ca.kit.edu/p/server",
		},
	},
	distribute: {
		title: "Zertifikate verteilen",
		available: {
			header: "Hinterlegte Zertifikate",
			createdAt: "Hinterlegt am {localeString}",
			noResults: "Keine Zertifikate hinterlegt",
			download: "Herunterladen",
		},
		errors: {
			downloadFailed: "Herunterladen des Zertifikats fehlgeschlagen",
		},
	},
	notFound: {
		title: "404 - nicht gefunden",
		message: "Seite nicht gefunden",
	},
	maintenance: {
		title: "Wartungsmodus",
		header: "Das Zertifikatsportal ist im Wartungsmodus",
		message:
			"Aktuell ist über dieses Portal noch keine Zertifikatsausstellung möglich. Bitte verwenden Sie den verlinkten Übergangsprozess.",
		infoButton: "Übergangsprozess",
	},
	errors: {
		requestIdLabel: "Anfragen-ID",
		api: {
			title: "Anfrage fehlgeschlagen",
		},
		backend: {
			title: "Kritischer Serverfehler aufgetreten",
			sorry: "Das hätte nicht passieren sollen.",
			contact: "Bitte kontaktieren sie {mail} wenn das Problem länger besteht.",
			requestIdHint: "Bitte füge unbedingt die unten angezeigte Anfragen-ID bei.",
		},
		unknown: "Unbekannter Fehler aufgetreten.",
		backendGone: "Der KIT-CA-Server ist nicht errreichbar.",
		invalidAuthHeader: "Authentisierungs-Kopfzeile hatte das falsche Format.",
		invalidAuthToken: "Authentisierungsmerkmal war ungültig oder abgelaufen.",
		ldapSearchSizeLimitExceeded: "Der Suchbegriff führt zu zu vielen Ergebnissen, bitte genauer spezifizieren.",
		problemFieldEmpty: "Das Feld darf nicht leer sein.",
		problemFieldTooLong: "Inhalt ist zu lang.",
		problemFieldInvalid: "Inhalt ist ungültig.",
		accountIsPseudonym:
			"Dieser Account is pseudonymisiert. Bitte sagen Sie dem Nutzenden, dass der Account entweder depseudonymisiert werden muss oder alternativ ein Funktionszertifikat ohne Identifizierung angefordert werden kann.",
		noIdentificationFound: "Die für den Zertifikatsantrag verwendete Identifikation konnte nicht gefunden werden.",
		accountDoesNotMatch: "Die für den Zertifikatsantrag verwendete Identifikation für eine andere Person angelegt.",
		notYourEmailAddress:
			"In Ihrem Zertifikatsantrag ist eine Mailadresse enthalten, die nicht Ihnen zugeordnet ist.",
		malformedData: "Der Bezeichner des hinterlegten Zertifikats ist ungültig.",
	},
};
