import { I18nType } from "@/i18n/type";

export const en: I18nType = {
	$vuetify: {
		stepper: {
			prev: "Previous",
			next: "Next",
			submit: "Submit",
		},
	},
	dialog: {
		nextButton: "Next",
		abortButton: "Abort",
		saveButton: "Save",
		closeButton: "Close",
		moreInfoButton: "More Info",
	},
	systemMessage: {
		title: "No certificate issuance possible",
		message:
			"Sectigo, the operator of the certification authority that issues our certificates, removed access for us on January 10, 2025. Therefore, no certificates can be requested at the moment until the new operator is properly integrated. This may take a few weeks, we ask for your understanding and are sorry for any inconvenience caused.",
	},
	unsupportedLocale: {
		title: "Unsupported Language",
		message:
			"Your browsers language {language} is not yet supported by this portal. You are viewing the default {defaultLanguage} version.",
		contribute: "Contribute translation",
		dontShowAgain: "Don't show again",
	},
	home: {
		title: "Certificate Portal",
		welcomeMessage: "Welcome, {name}!",
		sessionExpired: {
			moreInfo: {
				title: "Session Expiration Information",
			},
			message: "Session expired, you have been logged out.",
		},
		identificationSectionHeader: "Identification",
		requestSectionHeader: "Request Certificates",
		manageSectionHeader: "Manage Certificates",
		loginButton: "Login",
		logoutButton: "Logout",
		certificates: {
			request: {
				button: {
					enabled: "Request",
					disabled: "Not available",
				},
				personal: {
					name: "Personal Certificate",
					description:
						"This allows you to request a personal certificate, e.g. for signing and encrypting E-Mails or signing documents. A personal certificate is bound to your person and asserted by an identification.",
				},
				function: {
					name: "Functional Certificate",
					description:
						"This allows you to request a functional certificate, e.g. for signing and encrypting E-Mails from a shared mailbox. No prior identification is necessary, you will just need to validate that you have control over the mail address that you want to certify. It can therefore also be used as a replacement for a personal certificate if you do not need your name on the certificate.",
				},
				server: {
					name: "Server Certificate",
					description:
						"This allows you to request a server certificate necessary for secure communication between clients and servers or servers and servers. You will need to prove that you control the domain name that you want to certify. Currently, either ACME can be used or a certificate request can be sent by email.",
				},
			},
			manage: {
				button: {
					enabled: "Select",
					disabled: "Not available",
				},
				distribute: {
					name: "Distribute Certificates",
					description: "This allows you to distribute certificates securely to multiple devices.",
				},
			},
		},
		identification: {
			button: {
				enabled: "Select",
				disabled: "Not available",
			},
			view: {
				name: "View Identification",
				description:
					"This allows you to view identifications that exist and revoke identifications that you conducted yourself in the past.",
			},
			create: {
				name: "Create Identification",
				description:
					"This allows you to create a new identification for a member of KIT. You will need to check an official document of them.",
			},
		},
	},
	functionPage: {
		backHomeButton: "Back to main page",
		startOverButton: "Start Over",
	},
	createIdent: {
		title: "Create Identification",
		search: {
			header: "Search for KIT members",
			label: "Search Term",
			noResults: "No results found",
			result: {
				moreDetails: {
					kitAccountLabel: "KIT ID",
					commonNameLabel: "Common Name",
					firstNameLabel: "First Name",
					lastNameLabel: "Last Name",
					departmentLabel: "Department",
					primaryEmailAddressLabel: "Primary E-Mail Address",
					emailAddressLabel: "E-Mail Address",
				},
				moreDetailsButton: "Details",
				identifyButton: "Identify",
			},
			rules: {
				noSpacesAllowed: "Spaces are not allowed",
				invalidKitAccount: "Invalid KIT ID format",
			},
		},
		create: {
			title: "Create identification for {name}",
			existing: {
				title: "{name} already has valid identifications",
				createdAtLabel: "Created at",
				validUntilLabel: "Valid until",
				description:
					"Please ask the KIT member if they still want to create a new identification. New identifications make sense if they recently changed their name because of a doctorate degree, wedding, transition, ...",
			},
			success: {
				title: "Successfully created new identification for {name}!",
				description: "There are now the following identifications:",
			},
			error: {
				title: "Errors occurred while creating new identification:",
			},
			documentType: {
				selectTitle: "Document Type",
				unknown: "Unknown",
				personalausweis: "Identity Card",
				passport: "Passport",
				aufenthaltstitel: "Residence Permit",
				other: "Other Document (please add remark)",
			},
			documentTypeHint:
				"For information about any document, like safety features to check, click here and search.",
			documentIdentifier: "Document Identifier (last 5 characters)",
			documentIdentifierHint: {
				title: "Information about the Document Identifier",
				text: "This picture shows where document identifiers are on the german version of the selected document. Other countries are often similar. If you can't find it, you can check the PRADO database.",
			},
			commonName: "Common Name (shown in certificates)",
			commonNameHint:
				"Currently not changeable, generated from IDM. Please compare to identity document. If there are different, cancel the identification process and redirect user to PSE for changes.",
			auditorRemarks: "Remarks",
			auditorRemarksEmptyWithDocTypeOtherError: 'Remarks can\'t be empty if document type is "Other Document"',
		},
	},
	request: {
		common: {
			steps: {
				emailAddresses: {
					title: "Email Addresses",
				},
				checkRequest: {
					title: "Check & Submit Certificate Request",
					requestInfo: {
						primaryEmailAddress: "Primary (visible) email address of the certificate",
						emailAddress: "Additional email address included in the certificate",
					},
				},
			},
			success: {
				title: "Your certificate request was successfully submitted",
				text: "Your application will be reviewed in the next days. Afterwards, you will receive an email with further instructions. If you wish, you can already familiarize yourself with the next steps.",
				instructionsUrl: "https://ca.kit.edu/p/user#navigate-the-certificate-generation-process-by-sectigo",
				instructionsButton: "Next steps",
			},
			error: {
				title: "An error occurred",
				serviceDeskButton: "Service Desk",
				serviceDeskUrl: "https://www.scc.kit.edu/en/aboutus/116.php#Anker0",
			},
		},
		personal: {
			title: "Request Personal Certificate",
			missingIdentification: {
				title: "Missing Identification",
				text: "We do not have a valid identification for you. Please visit the SCC service desk with a valid identity document for identification. Afterwards, you can try applying for a personal certificate again. Alternatively, you can apply for a functional certificate.",
			},
			steps: {
				emailAddresses: {
					description:
						"Please select the email addresses to be included in your certificate. Your primary address will always be included in the certificate and cannot be removed.",
				},
				checkRequest: {
					description:
						"Please check your certificate request for accuracy. If something is wrong, your data will need to be corrected in the identity management system. In this case, please contact the SCC Service Desk.",
					requestInfo: {
						commonName: "Name used in the certificate",
					},
				},
			},
			error: {
				text: "For assistance, you can contact the SCC Service Desk. If you are fine with your name not appearing in the certificate, you can apply for a functional certificate instead.",
				functionalInsteadButton: "Functional Certificate",
			},
		},
		functional: {
			title: "Request Functional Certificate",
			steps: {
				emailAddresses: {
					description:
						"Please enter the email addresses to be included in the certificate. All email addresses must be verifiable by us and belong to the same mailbox. For email addresses of different mailboxes, separate certificates must be requested. For functional mailboxes, the account currently logged in to the CA portal must have access permissions to the mailbox or must be an IT officer of the OU assigned to the mailbox.",
					emailAddressTextFieldLabel: "Email Address",
					addAnotherButton: "Add another email address",
					addAnotherButtonShort: "Add",
					rules: {
						emailAddressEmpty: "The email address cannot be empty",
						emailAddressDoesNotContainExactlyOneAtSign: "The email address must contain exactly one {'@'}",
						emailAddressEndsWithAtSign: "The email address cannot end with an {'@'}",
						emailAddressContainsWhitespace:
							"The email address cannot contain any spaces or other whitespace",
						emailAddressContainsNonASCII:
							"The email address can only contain ASCII characters (no umlauts, emojis, ...)",
						emailAddressAlreadyExists: "The email address is given multiple times",
					},
				},
				checkRequest: {
					description:
						"Please check your certificate request for accuracy. If something is incorrect, go one step back and correct your entries.",
				},
			},
			error: {
				text: "For assistance, you can contact the SCC Service Desk.",
			},
		},
		server: {
			instructionsUrl: "https://ca.kit.edu/p/server-en",
		},
	},
	distribute: {
		title: "Distribute Certificates",
		available: {
			header: "Stored Certificates",
			createdAt: "Stored on {localeString}",
			noResults: "No stored certificates",
			download: "Download",
		},
		errors: {
			downloadFailed: "Failed to download certificate",
		},
	},
	notFound: {
		title: "404 - not found",
		message: "Page not found",
	},
	maintenance: {
		title: "Maintenance Mode",
		header: "Certificate Portal is in Maintenance Mode",
		message:
			"Certificate issuance via this portal is not ready yet. Please use the intermediary process linked below.",
		infoButton: "Intermediary Process",
	},
	errors: {
		requestIdLabel: "Request ID",
		api: {
			title: "Request failed",
		},
		backend: {
			title: "Critical backend error occurred",
			sorry: "This should not have happened.",
			contact: "Please contact {mail} if this issue persists.",
			requestIdHint: "Make sure to include the request ID shown below.",
		},
		unknown: "Unknown error occurred.",
		backendGone: "The KIT-CA server is not reachable.",
		invalidAuthHeader: "Authentication Header was malformed.",
		invalidAuthToken: "Authentication token was invalid or expired.",
		ldapSearchSizeLimitExceeded: "Your search query lead to too many results, please specify it better.",
		problemFieldEmpty: "This field can't be empty.",
		problemFieldTooLong: "Content is too long.",
		problemFieldInvalid: "Content is invalid.",
		accountIsPseudonym:
			"The account is pseudonymous, identification does not make sense. Please tell the user to either depseudonymize the account or request a functional certificate without identification.",
		noIdentificationFound: "The identification used for the certificate request could not be found.",
		accountDoesNotMatch: "The identification used for the certificate request was created for another person.",
		notYourEmailAddress: "Your certificate request contains an email address that is not assigned to you.",
		malformedData: "The identifier of the stored certificate is invalid.",
	},
};
