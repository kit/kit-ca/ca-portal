export type I18nType = {
	$vuetify: {
		stepper: {
			prev: string;
			next: string;
			submit: string;
		};
	};
	dialog: {
		nextButton: string;
		abortButton: string;
		saveButton: string;
		closeButton: string;
		moreInfoButton: string;
	};
	systemMessage: {
		title: string;
		message: string;
	};
	unsupportedLocale: {
		title: string;
		message: string;
		contribute: string;
		dontShowAgain: string;
	};
	home: {
		title: string;
		welcomeMessage: string;
		identificationSectionHeader: string;
		requestSectionHeader: string;
		manageSectionHeader: string;
		loginButton: string;
		logoutButton: string;
		sessionExpired: {
			moreInfo: {
				title: string;
			};
			message: string;
		};
		certificates: {
			request: {
				button: {
					enabled: string;
					disabled: string;
				};
				personal: {
					name: string;
					description: string;
				};
				function: {
					name: string;
					description: string;
				};
				server: {
					name: string;
					description: string;
				};
			};
			manage: {
				button: {
					enabled: string;
					disabled: string;
				};
				distribute: {
					name: string;
					description: string;
				};
			};
		};
		identification: {
			button: {
				enabled: string;
				disabled: string;
			};
			view: {
				name: string;
				description: string;
			};
			create: {
				name: string;
				description: string;
			};
		};
	};
	functionPage: {
		backHomeButton: string;
		startOverButton: string;
	};
	createIdent: {
		title: string;
		search: {
			header: string;
			label: string;
			noResults: string;
			rules: {
				noSpacesAllowed: string;
				invalidKitAccount: string;
			};
			result: {
				moreDetails: {
					kitAccountLabel: string;
					commonNameLabel: string;
					firstNameLabel: string;
					lastNameLabel: string;
					departmentLabel: string;
					primaryEmailAddressLabel: string;
					emailAddressLabel: string;
				};
				moreDetailsButton: string;
				identifyButton: string;
			};
		};
		create: {
			title: string;
			existing: {
				title: string;
				createdAtLabel: string;
				validUntilLabel: string;
				description: string;
			};
			success: {
				title: string;
				description: string;
			};
			error: {
				title: string;
			};
			documentType: {
				selectTitle: string;
				unknown: string;
				personalausweis: string;
				passport: string;
				aufenthaltstitel: string;
				other: string;
			};
			documentTypeHint: string;
			documentIdentifier: string;
			documentIdentifierHint: {
				title: string;
				text: string;
			};
			commonName: string;
			commonNameHint: string;
			auditorRemarks: string;
			auditorRemarksEmptyWithDocTypeOtherError: string;
		};
	};
	request: {
		common: {
			steps: {
				emailAddresses: {
					title: string;
				};
				checkRequest: {
					title: string;
					requestInfo: {
						primaryEmailAddress: string;
						emailAddress: string;
					};
				};
			};
			success: {
				title: string;
				text: string;
				instructionsUrl: string;
				instructionsButton: string;
			};
			error: {
				title: string;
				serviceDeskButton: string;
				serviceDeskUrl: string;
			};
		};
		personal: {
			title: string;
			missingIdentification: {
				title: string;
				text: string;
			};
			steps: {
				emailAddresses: {
					description: string;
				};
				checkRequest: {
					description: string;
					requestInfo: {
						commonName: string;
					};
				};
			};
			error: {
				text: string;
				functionalInsteadButton: string;
			};
		};
		functional: {
			title: string;
			steps: {
				emailAddresses: {
					description: string;
					emailAddressTextFieldLabel: string;
					addAnotherButton: string;
					addAnotherButtonShort: string;
					rules: {
						emailAddressEmpty: string;
						emailAddressDoesNotContainExactlyOneAtSign: string;
						emailAddressEndsWithAtSign: string;
						emailAddressContainsWhitespace: string;
						emailAddressContainsNonASCII: string;
						emailAddressAlreadyExists: string;
					};
				};
				checkRequest: {
					description: string;
				};
			};
			error: {
				text: string;
			};
		};
		server: {
			instructionsUrl: string;
		};
	};
	distribute: {
		title: string;
		available: {
			header: string;
			createdAt: string;
			noResults: string;
			download: string;
		};
		errors: {
			downloadFailed: string;
		};
	};
	notFound: {
		title: string;
		message: string;
	};
	maintenance: {
		title: string;
		header: string;
		message: string;
		infoButton: string;
	};
	errors: {
		requestIdLabel: string;
		api: {
			title: string;
		};
		backend: {
			title: string;
			sorry: string;
			contact: string;
			requestIdHint: string;
		};
		unknown: string;
		backendGone: string;
		invalidAuthHeader: string;
		invalidAuthToken: string;
		ldapSearchSizeLimitExceeded: string;
		problemFieldEmpty: string;
		problemFieldTooLong: string;
		problemFieldInvalid: string;
		accountIsPseudonym: string;
		noIdentificationFound: string;
		accountDoesNotMatch: string;
		notYourEmailAddress: string;
		malformedData: string;
	};
};
