// Styles
import "@fortawesome/fontawesome-free/css/all.css";
import "vuetify/styles";

// Components
import App from "./App.vue";

// Composables
import { createApp } from "vue";

// Plugins
import { createVuetify } from "vuetify";
import { aliases, fa } from "vuetify/iconsets/fa";
import { createVueI18nAdapter } from "vuetify/locale/adapters/vue-i18n";
import { createI18n, useI18n } from "vue-i18n";
import router from "@/router";
import { en } from "@/i18n/en";
import { de } from "@/i18n/de";

const i18n = createI18n({
	legacy: false,
	locale: navigator.language.split("-")[0],
	fallbackLocale: "en",
	messages: {
		en,
		de,
	},
});

const vuetify = createVuetify({
	theme: {
		themes: {
			light: {
				colors: {
					primary: "#009682",
					secondary: "#4664aa",
					kitblack: "#404040",
					kitbrown: "#a7822e",
					kitpurple: "#a3107c",
					kitcyan: "#23a1e0",
					kitmaygreen: "#77a200",
				},
			},
			dark: {
				colors: {
					primary: "#009682",
					secondary: "#4664aa",
					kitblack: "#404040",
					kitbrown: "#a7822e",
					kitpurple: "#a3107c",
					kitcyan: "#23a1e0",
					kitmaygreen: "#77a200",
				},
			},
		},
	},
	icons: {
		defaultSet: "fa",
		aliases,
		sets: {
			fa,
		},
	},
	display: {
		thresholds: {},
	},
	locale: {
		adapter: createVueI18nAdapter({ i18n, useI18n }),
	},
});

const app = createApp(App);

app.use(i18n);
app.use(vuetify);
app.use(router);

app.mount("#app");
