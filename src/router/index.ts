import { createRouter, createWebHistory } from "vue-router";
import { MAINTENANCE_MODE, store } from "@/ts/common";

const routes = [
	{
		path: "/",
		component: () => import("@/layouts/Layout.vue"),
		children: [
			{
				path: "/maintenance",
				name: "maintenance",
				component: () => import("@/views/Maintenance.vue"),
			},
			{
				path: "",
				name: "home",
				component: () => import("@/views/Home.vue"),
			},
			{
				path: "create-ident",
				name: "createIdent",
				component: () => import("@/views/CreateIdent.vue"),
			},
			{
				path: "request-personal",
				name: "request.personal",
				component: () => import("@/views/RequestPersonal.vue"),
			},
			{
				path: "request-functional",
				name: "request.functional",
				component: () => import("@/views/RequestFunctional.vue"),
			},
			{
				path: "/:pathMatch(.*)*",
				name: "notFound",
				component: () => import("@/views/NotFound.vue"),
			},
			{
				path: "distribute",
				name: "distribute",
				component: () => import("@/views/Distribute.vue"),
			},
		],
	},
];

const router = createRouter({
	history: createWebHistory(process.env.BASE_URL),
	routes,
});

router.beforeEach(async (from, _, next) => {
	const maintenanceRoute = { name: "maintenance" };
	if (MAINTENANCE_MODE.enabled && from.name !== "maintenance") {
		// Login not allowed, everything to maintenance
		if (!MAINTENANCE_MODE.allowLogin) {
			next(maintenanceRoute);
			return;
		}

		// Logged in, but maintenance ignore is disallowed or the user is not allowed to ignore maintenance mode
		// -> everything to maintenance
		if (
			store.user != null &&
			(!MAINTENANCE_MODE.allowIgnoreMaintenance || !store.user.allowedToIgnoreMaintenance)
		) {
			next(maintenanceRoute);
			return;
		}

		// Accept everything else
		next();
	} else {
		next();
	}
});

export default router;
