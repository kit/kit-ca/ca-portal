import { MaintenanceMode, ResponseWrapper, StoreType, TokenPayload, User } from "@/ts/types";
import { APIError } from "@/ts/errorTypes";
import { reactive } from "vue";
import router from "@/router";
import { jwtDecode } from "jwt-decode";

const API_PATH = "/api/v1";
const MODULES = import.meta.glob("/src/assets/*", { eager: true });
export const MAINTENANCE_MODE: MaintenanceMode = {
	enabled: false,
	allowLogin: true,
	allowIgnoreMaintenance: true,
	infoUrl: {
		en: "https://ca.kit.edu/p/user",
		de: "https://ca.kit.edu/p/nutzer",
	},
};
export const SHOW_SYSTEM_MESSAGE: boolean = true;

export const store: StoreType = reactive({
	setTitle: (newTitle: string) => {
		document.title = newTitle;
	},
	logoUrl: "",
	token: null,
	user: null,
	logoutError: null,
	apiErrorDialogData: null,
	backendError: null,
});

function parseTokenPayload(token: string | null): TokenPayload | null {
	try {
		return token ? jwtDecode<TokenPayload>(token) : null;
	} catch (err) {
		return null;
	}
}

export async function loadAndCheckToken() {
	if (!localStorage.getItem("authtoken") && !store.token) {
		// Token does not exist, immediately log out without checking the token
		await logout();
		return;
	}

	store.token = localStorage.getItem("authtoken");
	await requestBackend<null>("/token-check").then(
		() => {
			const tokenPayload = parseTokenPayload(store.token);
			if (!tokenPayload) {
				logout();
				return;
			}

			if (!store.user) {
				store.user = new User(tokenPayload);
			}
		},
		() => {
			logout();
		}
	);
}

export function login() {
	store.logoutError = null;
	location.replace("/login");
}

let currentlyReroutingToHome = false;
export async function logout() {
	localStorage.removeItem("authtoken");
	store.token = null;
	store.user = null;

	const currentRoute = router.currentRoute.value.name;
	if (currentRoute && currentRoute !== "home") {
		// Prevent indefinite reroute because token check is done before each route enter
		if (currentlyReroutingToHome) {
			return;
		}

		currentlyReroutingToHome = true;
		await router
			.push({
				name: "home",
			})
			.finally(() => {
				currentlyReroutingToHome = false;
			});
	}
}

export function checkRuleResult(result: boolean | string) {
	return typeof result == "string" ? false : result;
}

export function getAssetUrl(assetName: string) {
	const mod = MODULES[`/src/assets/${assetName}`] as { default: string };
	return mod?.default ?? "";
}

export function getLocaleFlagUrl(locale: string) {
	return getAssetUrl(`flag-${locale}.svg`);
}

export function kebapToCamelCase(kebapCaseString: string) {
	return kebapCaseString.replace(/-./g, (match) => match[1].toUpperCase());
}

export async function requestBackend<ResponseType>(url: string, body: object | null = null): Promise<ResponseType> {
	return requestBackendWithResponseHeaders<ResponseType>(url, body).then((wrappedResponse) => {
		return wrappedResponse.response;
	});
}

export async function requestBackendWithResponseHeaders<ResponseType>(
	url: string,
	body: object | null = null,
	returnBlob: boolean = false
): Promise<ResponseWrapper<ResponseType>> {
	let cleanedUrl = url;
	if (!cleanedUrl.startsWith("/")) {
		cleanedUrl = "/" + url;
	}

	// Load token if it is not loaded already
	if (!store.token) {
		await loadAndCheckToken();
	}

	const init: RequestInit = {
		headers: {
			Authorization: `Bearer ${store.token}`,
		},
	};

	if (body) {
		init.method = "POST";
		init.headers = { ...init.headers, ...{ "Content-Type": "application/json" } };
		init.body = JSON.stringify(body);
	}

	return fetch(`${API_PATH}${cleanedUrl}`, init).then(async (response) => {
		let parsedBody;
		if (response.headers.get("content-type") === "application/json") {
			parsedBody = await response.json();
		} else if (returnBlob) {
			parsedBody = await response.blob();
		} else {
			parsedBody = await response.text();
		}

		if (response.ok) {
			return new ResponseWrapper<ResponseType>(parsedBody, response.headers);
		}

		const apiError = new APIError(parsedBody);
		if (response.status === 401) {
			store.logoutError = apiError;
			await logout();
			throw apiError;
		} else if (response.status >= 500) {
			if (response.status === 502) {
				store.backendError = new APIError({
					name: "backend-gone",
				});
				return Promise.reject();
			} else {
				store.backendError = apiError;
				return Promise.reject();
			}
		}

		throw apiError;
	});
}
