import { kebapToCamelCase } from "@/ts/common";
import { TranslatorFunction } from "@/ts/types";

const getLocalized = function (t: TranslatorFunction, identifier: string, fallback: string | undefined): string {
	if (!identifier) {
		return fallback || t("errors.unknown");
	}

	const fullIdentifier = `errors.${kebapToCamelCase(identifier.replace(/^error-/, ""))}`;
	const translated = t(fullIdentifier);

	// Return something more useful if the translation failed (i.e., the i18n identifier came back as translation)
	if (translated === fullIdentifier) {
		return fallback || t("errors.unknown");
	}

	return translated;
};

export type APIErrorType = {
	name: string;
	request_id?: string;
	description?: string;
};

export class APIError {
	name: string;
	requestId?: string;
	description?: string;

	constructor(error: APIErrorType) {
		this.name = error.name;
		this.requestId = error.request_id;
		this.description = error.description;
	}

	getLocalized(t: TranslatorFunction): string {
		return getLocalized(t, this.name, this.description);
	}
}

export class APIErrorDialogData {
	prefixI18nId: string;
	apiError: APIError;

	constructor(prefixI18nId: string, apiError: APIError) {
		this.prefixI18nId = prefixI18nId;
		this.apiError = apiError;
	}
}

export type IdentificationValidityErrorType = {
	field: string;
	reason: string;
};

export class IdentificationValidityError {
	errors: Array<IdentificationValidityErrorType>;

	constructor(errors: Array<IdentificationValidityErrorType>) {
		this.errors = errors;
	}

	getField(field: string): IdentificationValidityErrorType | undefined {
		return this.errors.find((entry) => entry.field === field);
	}

	checkFieldExists(field: string): boolean {
		return !!this.getField(field);
	}

	getLocalized(t: TranslatorFunction, field: string): string {
		const entry = this.getField(field);
		if (!entry) {
			return t("errors.unknown");
		}

		return getLocalized(t, entry.reason, entry.reason);
	}
}
