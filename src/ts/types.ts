import { APIError, APIErrorDialogData } from "@/ts/errorTypes";
import { UnwrapNestedRefs } from "vue";

export type TranslatorFunction = (key: string) => string;
export type RulesType = { [key: string]: (value: string) => boolean | string };
export type RuleType = (value: string) => boolean | string;
export type VFormType = { validate: () => boolean };

export type StoreType = UnwrapNestedRefs<{
	setTitle: (newTitle: string) => void;
	logoUrl: string;
	token: string | null;
	user: User | null;
	logoutError: APIError | null;
	apiErrorDialogData: APIErrorDialogData | null;
	backendError: APIError | null;
}>;

export type TokenPayload = {
	iss: string;
	iat: number;
	sub: string;
	nbf: number;
	exp: number;
	firstname: string;
	lastname: string;
	displayname: string;
	primary_email: string;
	auxiliary_emails: Array<string>;
	memberof: Record<string, boolean>;
};

export class User {
	firstname: string;
	lastname: string;
	displayname: string;
	primaryEmail: string;
	auxiliaryEmails: Array<string>;
	allowedToIdentify: boolean;
	allowedToIgnoreMaintenance: boolean;

	constructor(tokenPayload: TokenPayload) {
		this.firstname = tokenPayload.firstname;
		this.lastname = tokenPayload.lastname;
		this.displayname = tokenPayload.displayname;
		this.primaryEmail = tokenPayload.primary_email;
		this.auxiliaryEmails = tokenPayload.auxiliary_emails ?? [];
		this.allowedToIdentify = tokenPayload.memberof["SCC-KIT-CA-TCS-RA"];
		this.allowedToIgnoreMaintenance = tokenPayload.memberof["SCC-KIT-CA-TCS-RA"];
	}
}

export type APILDAPSearchResultType = {
	results: Array<LDAPSearchResultType>;
};

export type LDAPSearchResultType = {
	dn: string;
	sAMAccountName: string;
	givenName: string;
	sn: string;
	displayName: string;
	department: string;
	cn: string;
	primaryEmail: string;
	emailAddresses: Array<string>;
};

export class LDAPSearchResult {
	kitAccount: string;
	distinguishedName: string;
	commonName: string;
	firstName: string;
	lastName: string;
	department: string;
	displayName: string;
	primaryEmail: string;
	emailAddresses: Array<string>;

	constructor(ldapSearchResult: LDAPSearchResultType) {
		this.kitAccount = ldapSearchResult.sAMAccountName;
		this.distinguishedName = ldapSearchResult.dn;
		this.commonName = ldapSearchResult.cn;
		this.firstName = ldapSearchResult.givenName;
		this.lastName = ldapSearchResult.sn;
		this.department = ldapSearchResult.department;
		this.displayName = ldapSearchResult.displayName;
		this.primaryEmail = ldapSearchResult.primaryEmail;
		this.emailAddresses = ldapSearchResult.emailAddresses;
	}

	getFullName(): string {
		return `${this.firstName} ${this.lastName}`;
	}

	getFullNameWithDepartment(): string {
		return `${this.getFullName()} (${this.department})`;
	}

	getAllMailAddresses(): Array<string> {
		let arrayWithPrimaryEmailAddress: Array<string> = [];
		if (this.primaryEmail) {
			arrayWithPrimaryEmailAddress = [this.primaryEmail];
		}

		return arrayWithPrimaryEmailAddress.concat(this.emailAddresses ?? []);
	}
}

export enum IdentificationMethod {
	KITCAG2 = "kit-ca-g2",
	ServicedeskSectigo = "scc-sd-sectigo",
}

export enum IdentificationDocumentTypeEnum {
	Unknown = "unknown",
	IdentityCard = "personalausweis",
	Passport = "passport",
	ResidencePermit = "aufenthaltstitel",
	Other = "other",
}
export interface IdentificationDocumentType {
	id: string;
	icon: string;
}

export const identificationDocumentTypeMap: Record<IdentificationDocumentTypeEnum, IdentificationDocumentType> = {
	[IdentificationDocumentTypeEnum.Unknown]: {
		id: IdentificationDocumentTypeEnum.Unknown,
		icon: "circle-question",
	},
	[IdentificationDocumentTypeEnum.IdentityCard]: {
		id: IdentificationDocumentTypeEnum.IdentityCard,
		icon: "id-card",
	},
	[IdentificationDocumentTypeEnum.Passport]: {
		id: IdentificationDocumentTypeEnum.Passport,
		icon: "passport",
	},
	[IdentificationDocumentTypeEnum.ResidencePermit]: {
		id: IdentificationDocumentTypeEnum.ResidencePermit,
		icon: "earth-asia",
	},
	[IdentificationDocumentTypeEnum.Other]: {
		id: IdentificationDocumentTypeEnum.Other,
		icon: "file-contract",
	},
};

export interface IdentificationType {
	public_id: string;
	common_name: string;
	created_at: number;
	valid_until: number;
	expired: boolean;
	revoked: boolean;
	revocation_date: number;
	document_type: string;
}

export interface ServiceDeskIdentificationType extends IdentificationType {
	public_id: string;
	kit_account: string;
	auditor_account: string;
	auditor_remarks: string;
	method: string;
}

export class Identification {
	publicId: string;
	commonName: string;
	createdAt: Date;
	validUntil: Date;
	expired: boolean;
	revoked: boolean;
	revocationDate: Date;
	documentType: IdentificationDocumentType;

	constructor(identification: IdentificationType) {
		const documentType = Object.values(IdentificationDocumentTypeEnum).find(
			(value) => value === identification.document_type
		);
		if (!documentType) {
			throw `Invalid document type ${identification.document_type}`;
		}

		this.publicId = identification.public_id;
		this.commonName = identification.common_name;
		this.createdAt = new Date(identification.created_at * 1000);
		this.validUntil = new Date(identification.valid_until * 1000);
		this.expired = identification.expired;
		this.revoked = identification.revoked;
		this.revocationDate = new Date(identification.revocation_date * 1000);
		this.documentType = identificationDocumentTypeMap[documentType];
	}
}

export class ServiceDeskIdentification extends Identification {
	publicId: string;
	kitAccount: string;
	auditorKitAccount: string;
	auditorRemarks: string;
	method: IdentificationMethod;

	constructor(identification: ServiceDeskIdentificationType) {
		super(identification);

		const method = Object.values(IdentificationMethod).find((value) => value === identification.method);
		if (!method) {
			throw `Invalid identification method ${identification.method}`;
		}

		this.publicId = identification.public_id;
		this.kitAccount = identification.kit_account;
		this.auditorKitAccount = identification.auditor_account;
		this.auditorRemarks = identification.auditor_remarks;
		this.method = method;
	}
}

export class IdentificationList<T extends IdentificationType, U extends Identification> {
	identifications: Array<U>;

	constructor(identifications: Array<T>, IdentificationClass: new (identification: T) => U) {
		this.identifications = identifications.map((entry) => new IdentificationClass(entry));
	}

	getValidIdentifications() {
		return this.identifications.filter((entry: U) => !entry.revoked && !entry.expired);
	}
}

export type NewIdentificationBody = {
	kit_account: string;
	common_name: string;
	document_identifier: string;
	document_type: IdentificationDocumentTypeEnum;
	auditor_remarks: string;
};

export type PKCS12Type = {
	id: string;
	filename: string;
	created_at: number;
};

export class PKCS12 {
	id: string;
	filename: string;
	createdAt: Date;

	constructor(pkcs12: PKCS12Type) {
		this.id = pkcs12.id;
		this.filename = pkcs12.filename;
		this.createdAt = new Date(pkcs12.created_at * 1000);
	}
}

export type MaintenanceInfoUrls = {
	en: string;
	de: string;
};

export type MaintenanceMode = {
	enabled: boolean;
	allowLogin: boolean;
	allowIgnoreMaintenance: boolean;
	infoUrl: MaintenanceInfoUrls;
};

export class ResponseWrapper<ResponseType> {
	response: ResponseType;
	headers: Headers;

	constructor(response: ResponseType, headers: Headers) {
		this.response = response;
		this.headers = headers;
	}
}
